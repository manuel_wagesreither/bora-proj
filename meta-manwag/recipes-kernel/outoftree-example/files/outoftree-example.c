/******************************************************************************
 *
 *   Copyright (C) 2011  Intel Corporation. All rights reserved.
 *
 *   This program is free software;  you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY;  without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 *   the GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program;  if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *****************************************************************************/

#include <linux/module.h>

static int __init outoftree_example_init(void)
{
	pr_info("Hello from an out-of-tree module.\n");
	return 0;
}

static void __exit outoftree_example_exit(void)
{
	pr_info("Goodbye from an out-of-tree module.\n");
}

module_init(outoftree_example_init);
module_exit(outoftree_example_exit);
MODULE_LICENSE("GPL");

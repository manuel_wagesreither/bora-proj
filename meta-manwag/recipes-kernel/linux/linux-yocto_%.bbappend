FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
    file://enable-intree-example.cfg \
    file://intree-example.patch \
"

KERNEL_MODULE_AUTOLOAD += "intree-example"

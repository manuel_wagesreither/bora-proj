DESCRIPTION = "Demonstrator package for udev rules"
LICENSE = "CLOSED"

RDEPENDS:${PN} = "\
        bash \
"

SRC_URI += "\
        file://udev-demonstrator.rules \
        file://udev-fired@.service \
        file://examine-disk@.service \
"

inherit systemd
SYSTEMD_SERVICE:${PN} = "\
        udev-fired@.service \
        examine-disk@.service \
"

SYSTEMD_AUTO_ENABLE = "enable"

do_install() {
        install -d                                                                   ${D}/lib/udev/rules.d
        install -c -m 0644 ${WORKDIR}/udev-demonstrator.rules                        ${D}/lib/udev/rules.d

        install -d                                                                   ${D}${systemd_system_unitdir}
        install -c -m 0644 ${WORKDIR}/udev-fired@.service                            ${D}${systemd_system_unitdir}
        install -c -m 0644 ${WORKDIR}/examine-disk@.service                          ${D}${systemd_system_unitdir}
}

FILES:${PN} += "\
        /lib/udev/rules.d/99-standalone-mender-deployment.rules \
        ${systemd_system_unitdir}/udev-fired@.service \
        ${systemd_system_unitdir}/examine-disk@.service \
"

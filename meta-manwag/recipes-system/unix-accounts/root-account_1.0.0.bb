SUMMARY = "Root user configuration"
LICENSE = "CLOSED"

SRC_URI = "\
    git://git@gitlab.com/manwag-infrastruktur/ssh-authorized-keys.git;protocol=ssh;branch=master \
    file://bashrc \
"
SRCREV = "${AUTOREV}"

# The git fetcher clones into 'git' subdir
S = "${WORKDIR}/git"

# ROOT_HOME is a predefined variable.
# http://docs.yoctoproject.org/ref-manual/variables.html#term-ROOT_HOME

do_compile() {
    sed --in-place \
        -e "s|@SBINDIR@|${sbindir}|g" \
        -e "s|@BASE_SBINDIR_NATIVE@|${base_sbindir_native}|g" \
        "${WORKDIR}/bashrc"
}

do_install () {
    install -d -m 755                         "${D}${ROOT_HOME}/.ssh"
    install -m 0644 "${S}/raspi"              "${D}${ROOT_HOME}/.ssh/authorized_keys"
    install -m 0644 "${WORKDIR}/bashrc"       "${D}${ROOT_HOME}/.bashrc"

    # chown/chgrp not necessary, as in Yocto file ownership defaults to root
}

FILES:${PN} += " \
    ${ROOT_HOME}/.bashrc \
    ${ROOT_HOME}/.ssh/authorized_keys \
"

# This package does not get compiled, hence
# splitting out debug info is not necessary.
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"

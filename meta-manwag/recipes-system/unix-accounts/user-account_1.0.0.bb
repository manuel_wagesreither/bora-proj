SUMMARY = "Creates an unix user account for interactive logins"
LICENSE = "CLOSED"

SRC_URI = "\
    git://git@gitlab.com/manwag-infrastruktur/ssh-authorized-keys.git;protocol=ssh;branch=master \
    file://bashrc \
"
SRCREV = "${AUTOREV}"

# The git fetcher clones into 'git' subdir
S = "${WORKDIR}/git"

USERNAME = "user"
# Generate password with `mkpasswd <my-password>`.
# "HbPVvBzE8bZSY" = "user"
PASSWORD = "HbPVvBzE8bZSY"

do_compile() {
    sed --in-place \
        -e "s|@SBINDIR@|${sbindir}|g" \
        -e "s|@BASE_SBINDIR_NATIVE@|${base_sbindir_native}|g" \
        "${WORKDIR}/bashrc"
}

do_install() {
    install -d                                "${D}/home/${USERNAME}/.ssh/"
    install -m 0644 "${S}/raspi"              "${D}/home/${USERNAME}/.ssh/authorized_keys"
    install -m 0644 "${WORKDIR}/bashrc"       "${D}/home/${USERNAME}/.bashrc"

    chown --recursive ${USERNAME}:${USERNAME} "${D}/home/${USERNAME}"
}

FILES:${PN} += " \
    /home/${USERNAME}/.bashrc \
    /home/${USERNAME}/.ssh/authorized_keys \
"

inherit useradd
USERADD_PACKAGES = "${PN}"
USERADD_PARAM:${PN} = "--password ${PASSWORD} --shell /bin/bash --groups sudo,video,dialout,tty,audio,input,lp ${USERNAME};"
# Necessary according to https://git.yoctoproject.org/cgit.cgi/poky/tree/meta-skeleton/recipes-skeleton/useradd/useradd-example.bb
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"

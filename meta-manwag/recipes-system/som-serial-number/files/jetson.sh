#!/bin/sh

# The serial number ends with a null-byte. Replace it with a new-
# line character to make this program a well-behaving unix citizen.
cat /proc/device-tree/serial-number | tr '\000' '\n'

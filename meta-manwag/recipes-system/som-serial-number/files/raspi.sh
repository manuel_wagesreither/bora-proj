#!/bin/sh

# The serial number ends with a null-byte. Replace it with a new-
# line character to make this program a well-behaving unix citizen.
cat /sys/firmware/devicetree/base/serial-number | tr '\000' '\n'

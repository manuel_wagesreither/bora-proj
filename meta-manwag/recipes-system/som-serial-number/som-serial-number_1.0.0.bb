DESCRIPTION = "Make the System-On-Modules serial number available in userspace"
LICENSE = "CLOSED"

# coreutils provides `tr`.
RDEPENDS:${PN} = "bash coreutils"

SRC_URI = "\
    file://raspi.sh \
    file://jetson.sh \
"

do_install() {
    :
}
do_install:append:rpi() {
    install -d                             "${D}${bindir}"
    install -m 0755 "${WORKDIR}/raspi.sh"  "${D}${bindir}/som-serial-number.sh"
}
do_install:append:tegra() {
    install -d                             "${D}${bindir}"
    install -m 0755 "${WORKDIR}/jetson.sh" "${D}${bindir}/som-serial-number.sh"
}

FILES:${PN} = ""
FILES:${PN}:append:rpi = " \
    ${bindir}/som-serial-number.sh \
"
FILES:${PN}:append:tegra = " \
    ${bindir}/som-serial-number.sh \
"

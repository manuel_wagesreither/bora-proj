# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# Unable to find any files that looked like license statements. Check the accompanying
# documentation and source headers and set LICENSE and LIC_FILES_CHKSUM accordingly.
#
# NOTE: LICENSE is being set to "CLOSED" to allow you to at least start building - if
# this is not accurate with respect to the licensing of the software being built (it
# will not be in most cases) you must specify the correct value before using this
# recipe for anything other than initial testing/development!
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

RDEPENDS:${PN} = "\
	python3-pygobject \
	python3 \
"

SRC_URI = "git://github.com/brettviren/pygst-tutorial-org.git;protocol=https;branch=master \
           file://0001-Fix-syntax-errors.patch \
"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "56101ed0c962fee406fad2199f3eff2389e9f819"

S = "${WORKDIR}/git"

do_compile[noexec] = "1"

do_install() {
	install -d                                         ${D}/opt/pygst-tutorial
	install -c -m 0755 ${S}/capabilities-example.py    ${D}/opt/pygst-tutorial/capabilities-example.py
}

FILES:${PN} += "\
	/opt/pygst-tutorial/capabilities-example.py \
"

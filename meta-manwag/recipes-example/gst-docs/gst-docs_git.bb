LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

DEPENDS = "gtk+3 gstreamer1.0 gstreamer1.0-plugins-base"

# git://github.com/GStreamer/gst-docs.git;protocol=https;nobranch=1;tag=1.19.2
SRC_URI = " \
    git://github.com/GStreamer/gst-docs.git;protocol=https;branch=master \
"
SRCREV = "ce5b31a1b0b953dbc147a7ab564d656739cdc792"
PV = "1.0+git${SRCPV}"

# Git fetcher clones into subdir 'git' by default
S = "${WORKDIR}/git"

inherit pkgconfig
do_compile() {
    ${CC} examples/tutorials/basic-tutorial-5.c -o ${B}/basic-tutorial-5 $( pkg-config --cflags --libs gstreamer-video-1.0 gtk+-3.0 gstreamer-1.0 ) ${LDFLAGS}
}

do_install() {
    install -d                            ${D}${bindir}
    install -m 0755 ${B}/basic-tutorial-5 ${D}${bindir}
}

FILES:${PN} = " \
    ${bindir}/basic-tutorial-5 \
"

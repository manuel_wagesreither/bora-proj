LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

DEPENDS = "openssl"

SRC_URI = "\
    git://gitlab.com/manuel_wagesreither/openssl-api-aes-gcm-usage-example.git;protocol=https;branch=master \
    file://run-ptest \
"
SRCREV = "c80fc74ee2394c40fb29136815b045b5b998863a"
PV = "1.0+git${SRCPV}"

# Git fetcher clones into subdir 'git' by default
S = "${WORKDIR}/git"

inherit cmake
EXTRA_OECMAKE = "-DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_FLAGS_DEBUG=-g"

inherit ptest
do_install_ptest() {
    install -m 0755 ${B}/src/crypto-api/cryptotest ${D}/${PTEST_PATH}
}

SUMMARY = "GStreamer element to imprint data set via GLib properties"
LICENSE = "CLOSED"

DEPENDS = "\
    gstreamer1.0 \
    gstreamer1.0-plugins-base \
"
RDEPENDS:${PN}-ptest = "bash"

SRC_URI = "\
    git://git@gitlab.com/manuel_wagesreither/gstreamer-elements.git;nobranch=1;protocol=ssh \
    file://run-ptest \
"
SRCREV = "4fdd9e956c4fc37b03b3f98a75abe047d2487d4d"

S = "${WORKDIR}/git"

inherit meson pkgconfig ptest

do_install_ptest() {
    install -m 0755 ${S}/src_propimprinter/*_test.bash    ${D}${PTEST_PATH}
}

FILES:${PN} += "\
    ${libdir}/gstreamer-1.0/*.so \
    ${libdir}/gstreamer-1.0/pkgconfig/gstpropimprinter.pc \
"

SUMMARY = "Example Python application using GTK+"
HOMEPAGE = "https://manuelwagesreither.com"
AUTHOR = "Manuel Wagesreither <ManWag@FastMail.FM>"
LICENSE = "CLOSED"

SRC_URI = "file://py-gtk-gui-1.0/"

RDEPENDS:${PN} = "${PYTHON_PN}-setuptools"

do_compile[noexec] = "1"

inherit setuptools3

"""A setuptools based setup module.

See:
https://packaging.python.org/guides/distributing-packages-using-setuptools/
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
from setuptools import setup
import pathlib

# Get the long description from the README file
here = pathlib.Path(__file__).parent.resolve()
long_description = (here / 'README.md').read_text(encoding='utf-8')

# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.

setup(
    name='py-gtk-gui',  # Required
    version='1.0.0',  # Required
    packages=['py_gtk_gui_package'],
    description='A sample Python project for testing Gtk',  # Optional
    long_description=long_description,  # Optional
    long_description_content_type='text/markdown',  # Optional
    url='https://gitlab.com/manuel_wagesreither/bora-proj',  # Optional
    author='Manuel Wagesreither',  # Optional
    author_email='ManWag@FastMail.FM',  # Optional
    python_requires='>=3.6, <4',

    entry_points={  # Optional
        'console_scripts': [
            'run-gui=py_gtk_gui_package:main',
        ],
    },

    # This field lists other packages that your project depends on to run.
    # Any package you put here will be installed by pip when your project is
    # installed, so they must be valid existing projects.
    #
    # For an analysis of "install_requires" vs pip's requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    # install_requires=['peppercorn'],  # Optional

)

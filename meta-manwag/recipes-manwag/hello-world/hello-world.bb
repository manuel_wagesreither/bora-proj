SUMMARY = "Example C++ application"
HOMEPAGE = "https://gitlab.com/manuel_wagesreither/hello-world"
AUTHOR = "Manuel Wagesreither <ManWag@FastMail.FM>"
LICENSE = "CLOSED"


do_fetch() {
    mkdir --parents ${S}/repo-content
    cd ${S}
    git clone "https://gitlab.com/manuel_wagesreither/hello-world.git" repo-content
}

do_patchhhh() {
    cd ${S}/repo-content
    patch -p1 < main.c.patch
}

do_configureeee() {
    mkdir --parents ${B}
    cd ${B}
    cmake "${S}" 
}

do_compile() {
    mkdir --parents "${B}/build"
    cd "${B}/build"
    # x86_64-poky-linux-gcc
    ${CC} ../main.c
}

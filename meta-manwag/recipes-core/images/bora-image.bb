DESCRIPTION = "A small image just capable of running simpleechoserver."
LICENSE = "MIT"

NICE_TO_HAVE = " \
    bash \
    bash-completion \
    cmake \
    git \
    gnupg \
    man \
    netcat-openbsd \
    ninja \
    rsync \
    sudo \
    tmux \
    tree \
    vim-tiny \
    xterm \
    xz \
    zip \
"

SYSTEM = "\
    devmem2 \
    evtest \
    fbset \
    file \
    htop \
    lsb-release \
    lsof \
    nfs-utils-client \
    openssh-sftp-server \
    os-release \
    pciutils \
    psmisc \
    systemd-analyze \
    usbutils \
    util-linux \
"
# This should work but breaks the build
#SYSTEM += " os-release-lic"

DISKTOOLS = " \
    dosfstools \
    e2fsprogs-badblocks \
    e2fsprogs-dumpe2fs \
    e2fsprogs-e2fsck \
    e2fsprogs-e2scrub \
    e2fsprogs-mke2fs \
    e2fsprogs-resize2fs \
    e2fsprogs-tune2fs \
    hdparm \
    mmc-utils \
    smartmontools \
"

NETWORKING = " \
    ethtool \
    iftop \
    iperf3 \
    iproute2 \
    obexftp \
    rfkill \
    ser2net \
    speedtest-cli \
"

GSTREAMER = " \
    gstreamer1.0 \
    gstreamer1.0-doc \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good \
    gstreamer1.0-plugins-bad \
"

GRAPHICS = "wayland weston weston-init weston-xwayland weston-examples"

# AUDIO = "alsa-utils-aplay"

GTK = "gtk+3-demo adwaita-icon-theme"

GRAPHICDEMOS = "mesa mesa-demos kmscube"

PLATFORMSPECIFIC = ""
PLATFORMSPECIFIC:append:qemuall = "kernel-module-intree-example"
PLATFORMSPECIFIC:append:rpi = " userland kernel-modules som-serial-number"

#    tegra-binaries
#    python3-jetcam
#    python3-jetson-stats
PLATFORMSPECIFIC:append:tegra = " \
    kernel-module-tegra-udrm tegra-udrm-probeconf \
    tegra-nvpmodel \
    tegra-tools-jetson-clocks \
    tegra-tools-tegrastats \
    tegra-firmware \
    tegra-boot-tools-nvbootctrl \
    setup-nv-boot-control \
    tegra-nvstartup \
    python3-pytools \
    som-serial-number \
"
PLATFORMSPECIFIC:append:tegra = " \
    cuda-command-line-tools \
    cuda-compiler \
    cuda-cuobjdump \
    cuda-libraries \
    cuda-nvml \
"

MY_PROJECTS = " \
    inline-cmake \
    openssl-api-crypt-dev openssl-api-crypt-dbg openssl-api-crypt-src openssl-api-crypt-ptest \
    pygst-tutorial-org \
    udev-rules \
    gst-propimprinter-ptest \
"

KERNEL_MODULES = "outoftree-example"

include payload.inc
# poky/meta/recipes-core/images/core-image-minimal.bb
# sets IMAGE_INSTALL according to this
CORE_IMAGE_EXTRA_INSTALL = " \
    ${NICE_TO_HAVE} \
    ${SYSTEM} \
    ${DISKTOOLS} \
    ${NETWORKING} \
    ${PAYLOAD} \
    ${GSTREAMER} \
    ${GRAPHICS} \
    ${GTK} \
    ${GRAPHICDEMOS} \
    ${PLATFORMSPECIFIC} \
    ${MY_PROJECTS} \
    ${KERNEL_MODULES} \
    ptest-runner \
    user-account root-account \
"

IMAGE_FEATURES = "\
    ssh-server-openssh \
    empty-root-password \
    allow-root-login \
    bash-completion-pkgs \
    debug-tweaks \
    tools-sdk \
    tools-debug \
    package-management \
"
require recipes-core/images/core-image-minimal.bb

# ---

inherit features_check
# Because X11 must be activated at the distro layer as well
REQUIRED_DISTRO_FEATURES = "x11"

# Some important package is pulled in only when opengl is added.
# Let's use it altough we don't need it.
# https://github.com/agherzan/meta-raspberrypi/issues/228
REQUIRED_DISTRO_FEATURES += "opengl"

require add-admin-user.inc
require enable-sudo-group.inc

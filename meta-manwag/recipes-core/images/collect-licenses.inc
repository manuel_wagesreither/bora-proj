# Worth a read:
# http://docs.yoctoproject.org/dev-manual/common-tasks.html#providing-license-text

# Copy the license manifest covering all installed packages into
# /usr/share/common-licenses/license.manifest.
# http://docs.yoctoproject.org/ref-manual/variables.html#term-COPY_LIC_MANIFEST
#COPY_LIC_MANIFEST = "1"

# Copy all installed packages license texts into /usr/share/common-licenses/<package>/.
# http://docs.yoctoproject.org/ref-manual/variables.html#term-COPY_LIC_DIRS
#COPY_LIC_DIRS = "1"

# Builds a ${PN}-lic package containting the license text for every package built.
# http://docs.yoctoproject.org/ref-manual/variables.html#term-LICENSE_CREATE_PACKAGE
#LICENSE_CREATE_PACKAGE = "1"

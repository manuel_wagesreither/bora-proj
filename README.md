# bora-proj

My personal yocto project.


## bora-container

* `tar tf build/tmp-glibc/deploy/images/containerx86-64/bora-container-containerx86-64-20210411040501.rootfs.tar.gz | less`
* `docker import build/tmp-glibc/deploy/images/containerx86-64/bora-container-containerx86-64-20210411040501.rootfs.tar.gz bora-container`
* `docker run --rm -ti -p 54321:12345 bora-container /usr/bin/simple_echo_server 12345`


## Flashing

* `sudo bmaptool copy --bmap bora-image-raspberrypi4-64.wic.bmap bora-image-raspberrypi4-64.wic.bz2 /dev/sda`


## Current bugs

### Qemu opengl passthrough

Error message:
```
runqemu - ERROR - Failed to run qemu: qemu-system-x86_64: ../libepoxy-1.5.4/src/dispatch_common.c:863: epoxy_get_proc_address: Assertion `0 && "Couldn't find current GLX or EGL context.\n"' failed.
```

Initial settings:
```
  qemu: |
    # For Dunfell
    PACKAGECONFIG:append:pn-qemu-system-native = " sdl virglrenderer glx"

    # In Hardknott, virglrenderer, glx get added automatically
    # http://git.yoctoproject.org/cgit/cgit.cgi/poky/commit/?id=7561eb32a01f7990ad4a4606ac97ebe2a659e029
    #PACKAGECONFIG:append:pn-qemu-system-native = " sdl gtk+"

    # Should get added automatically but isn't
    # https://git.openembedded.org/openembedded-core/tree/meta/recipes-graphics/libsdl2/libsdl2_2.0.16.bb
    #PACKAGECONFIG:append:pn-libsdl2-native = " opengl"
```

Log:

```
kas shell kas/bora-qemux.yml
bitbake bora-image
runqemu kvm slirp sdl gl
--> ERROR
vim conf/local.conf
--> opengl enabled
bitbake bora-image
runqemu kvm slirp sdl gl
--> ERROR
bitbake -c cleansstate libsdl2-native qemu-system-native
bitbake libsdl2-native qemu-system-native
runqemu kvm slirp sdl gl
--> ERROR
bitbake bora-image
runqemu kvm slirp sdl gl
--> ERROR
vim conf/local.conf
--> gtk+ enabled
bitbake bora-image
runqemu kvm slirp sdl gl
--> WORKS
```
```
kas shell kas/bora-qemux.yml
bitbake bora-image
vim conf/local.conf
--> opengl enabled
bitbake bora-image
runqemu kvm slirp sdl gl
--> ERROR
```
```
kas shell kas/bora-qemux.yml
vim conf/local.conf
--> opengl enabled
bitbake bora-image
runqemu kvm slirp sdl gl
--> WORKS
```
```
just shell qemux
bitbake bora-image
vim conf/local.conf
--> opengl enabled
bitbake bora-image
runqemu kvm slirp sdl gl
--> ERROR
```
```
just shell qemux
vim conf/local.conf
--> opengl enabled
bitbake bora-image
runqemu kvm slirp sdl gl
--> WORKS
```

### Qemuarm with graphics

* `runqemu - ERROR - Failed to run qemu: qemu-system-arm: Virtio VGA not available`
  * Perhaps [already fixed](https://patches.openembedded.org/patch/164351/).

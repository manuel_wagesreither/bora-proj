#!/usr/bin/env sh

if ! docker --version >/dev/null; then
	echo "This helper script needs the docker command available." >&2
	exit 1
fi

. ./.env

case $1 in
	create-builder)
		docker pull ghcr.io/siemens/kas/kas:$BORABUILDER_TAG
		docker tag ghcr.io/siemens/kas/kas:$BORABUILDER_TAG $BORABUILDER_IMAGE:$BORABUILDER_TAG
		#docker push $BORABUILDER_IMAGE:$BORABUILDER_TAG
		;;

	checkout)
		docker run --rm \
			--name $BORABUILDER_CONTAINER \
			--network "host" \
			--workdir "/work" \
			--mount type=bind,source="$(pwd)",target=/work \
			--env USER_ID=$(id -u) \
			--env GROUP_ID=$(id -g) \
			--env TERM \
			--env SHELL=/bin/bash \
			$BORABUILDER_IMAGE:$BORABUILDER_TAG \
			shell kas/bora-qemux86-64.yml -c 'exit'
		;;

	shell)
		docker run --rm -it \
			--name $BORABUILDER_CONTAINER \
			--network "host" \
			--workdir "/work" \
			--mount type=bind,source="$(pwd)",target=/work \
			--env USER_ID=$(id -u) \
			--env GROUP_ID=$(id -g) \
			--env TERM \
			--env SHELL=/bin/bash \
			$BORABUILDER_IMAGE:$BORABUILDER_TAG \
			shell kas/bora-qemux86-64.yml
		;;

	build)
		docker run --rm -it \
			--name $BORABUILDER_CONTAINER \
			--network "host" \
			--workdir "/work" \
			--mount type=bind,source="$(pwd)",target=/work \
			--env USER_ID=$(id -u) \
			--env GROUP_ID=$(id -g) \
			--env TERM \
			--env SHELL=/bin/bash \
			$BORABUILDER_IMAGE:$BORABUILDER_TAG \
			build kas/bora-qemux86-64.yml
		;;

	*)
		echo "Operation not supported" >&2
			echo "Syntax: $0 create-builder | checkout | shell | build" >&2
			exit 1
		;;
esac

#!/bin/sh
#set -x

image_dir=${1}

mkdir --parents "${image_dir}/netboot/overlays"

# Write config.txt
cat <<EOF > "${image_dir}/netboot/config.txt"
disable_overscan=1
kernel=kernel8.img
dtoverlay=vc4-fkms-v3d
arm_64bit=1
enable_uart=1
uart_2ndstage=1
dtoverlay=disable-bt
dtoverlay=rotary-encoder

# Enable Raspberry Pi camera
# meta-raspberrypi switch: RASPBERRYPI_CAMERA_V2
dtoverlay=imx219

# Enable v4l2 support for camera
# meta-raspberrypi switch: VIDEO_CAMERA
start_x=1

# Enable offline compositing
# meta-raspberrypi switch: DISPMANX_OFFLINE
dispmanx_offline=1
EOF

# Write cmdline.txt
# dwc_otg.lpm_enable=0 console=tty1 console=serial0,115200 root=/dev/sda2 rootfstype=ext4 rootwait
# dwc_otg.lpm_enable=0 console=tty1 console=serial0,115200 root=/dev/nfs nfsroot=192.168.128.1:/mnt/bora-root,vers=4.1,proto=tcp rw rootwait ip=dhcp
echo -n 'dwc_otg.lpm_enable=0 cma=64M console=tty1 console=serial0,115200 root=/dev/nfs nfsroot=192.168.128.1:/mnt/bora-root,vers=4.1,proto=tcp rw rootwait ip=dhcp' > "${image_dir}/netboot/cmdline.txt"

# Workaround to fix mouse lag (https://forums.raspberrypi.com/viewtopic.php?t=84999)
echo ' usbhid.mousepoll=0' >> "${image_dir}/netboot/cmdline.txt"

# Symlink bootfiles but skip cmdline.txt and config.txt to not overwrite those we created above.
find "${image_dir}/bootfiles/" -mindepth 1 -maxdepth 1 -not -name "config.txt" -not -name "cmdline.txt" -exec ln -srf "{}" "${image_dir}/netboot/" \;

# Link kernel
ln -srf "${image_dir}/Image" "${image_dir}/netboot/kernel8.img"

# Link device tree base
ln -srf "${image_dir}/bcm2711-rpi-4-b.dtb" "${image_dir}/netboot/"

# Link device tree overlays.
# For every overlay file, there are two convenience symlinks with shorter names available.
# Out of these three available options, we can identify the file with the shortest filename by the lack of 'raspberrypi4-64' in its name.
find "${image_dir}" -maxdepth 1 -name "*.dtbo" -not -name "*raspberrypi4-64*" -exec ln -srf "{}" "${image_dir}/netboot/overlays" \;

# Assign ip to the network interface destined for the target.
sudo ip a add 192.168.128.1/24 dev enx000ec6e204ed

# Start the tftp server
sudo systemctl start dnsmasq

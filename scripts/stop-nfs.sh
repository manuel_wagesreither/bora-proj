#!/bin/sh
#set -x

mountpoint="/mnt/bora-root"

if $( mount | grep --quiet -F "${mountpoint}" ); then
    sudo systemctl stop nfs-kernel-server
    sudo umount "${mountpoint}"
fi

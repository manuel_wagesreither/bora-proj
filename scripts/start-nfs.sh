#!/bin/sh
#set -x

image_dir=${1}
# filesystem="${image_dir}/core-image-minimal-raspberrypi4.ext3"
filesystem="${image_dir}/bora-image-raspberrypi4-64.ext3"

mountpoint="/mnt/bora-root"
mkdir --parents "${mountpoint}"

# If mounted, unmount the filesystem image to make sure we mount the latest one below.
if $( mount | grep --quiet -F "${mountpoint}" ); then
    sudo systemctl stop nfs-kernel-server
    sudo umount "${mountpoint}"
fi

sudo mount -t ext3 "${filesystem}" "${mountpoint}"
sudo systemctl start nfs-kernel-server

#!/bin/sh

[ -z "$USER_ID" ] && "This script needs \$USER_ID to be set." >&2 && exit 1
[ -z "$GROUP_ID" ] && "This script needs \$GROUP_ID to be set." >&2 && exit 1

HOME=/home/user
USER=user

groupadd -o --gid "$GROUP_ID" "$USER"
useradd -o --uid "$USER_ID" --gid "$GROUP_ID" --home-dir "$HOME" "$USER"

if [ -n "$KVM_GID" ]; then
    groupadd --gid "$KVM_GID" kvm
    usermod -aG kvm "$USER"
fi

# The double quotes around $@ are important.
# https://www.gnu.org/software/bash/manual/html_node/Special-Parameters.html
exec gosu "$USER" "$@"

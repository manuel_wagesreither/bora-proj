set dotenv-load := true

# Gets used in compose file
export USER_ID  := `id -u`
export GROUP_ID := `id -g`
export KVM_GID  := `getent group kvm | cut -d: -f3`
export XSOCK    := "/tmp/.X11-unix/X0"

BORABUILDER_COMPOSE := "docker/bora-builder.yaml"


# Print help text
help:
	@just --list --unsorted --list-prefix " →  "


# =========================================================


# Build image
build-container:
	docker-compose --file {{BORABUILDER_COMPOSE}} \
		build cmdservice


# =========================================================


# Checkout repos, clone if necessary.
checkout project="bora" machine="qemu":
	docker-compose --file {{BORABUILDER_COMPOSE}} \
		run --rm --name $BORABUILDER_CONTAINER \
		cmdservice kas checkout --skip setup_home kas/common.yml:kas/machine-{{machine}}.yml:kas/proj-{{project}}.yml


# Drop into kas shell inside container
shell project="bora" machine="qemu":
	docker-compose --file {{BORABUILDER_COMPOSE}} \
		run --rm --name $BORABUILDER_CONTAINER \
		xenabledservice kas shell --skip setup_home kas/common.yml:kas/machine-{{machine}}.yml:kas/proj-{{project}}.yml


# Like checkout target, but build images right away.
build project="bora" machine="qemu":
	docker-compose --file {{BORABUILDER_COMPOSE}} \
		run --rm --name $BORABUILDER_CONTAINER \
		cmdservice kas build --skip setup_home kas/common.yml:kas/machine-{{machine}}.yml:kas/proj-{{project}}.yml


# Run bora-image in qemu
qemu project="bora":
	docker-compose --file {{BORABUILDER_COMPOSE}} \
		run --rm --name $BORABUILDER_CONTAINER \
		xenabledservice kas shell --skip setup_home -c "runqemu kvm slirp sdl gl" kas/common.yml:kas/machine-qemu.yml:kas/proj-{{project}}.yml


# =========================================================


# Flash wic image
flash targetdevice machine="raspi":
	#!/bin/sh
	if [ "{{machine}}" = "raspi" ]; then sudo bmaptool copy "{{`just print-image-path raspi`}}/bora-image-raspberrypi4-64.wic.bz2" "/dev/{{targetdevice}}"; fi


# Drop into kas shell
kas project="bora" machine="qemu":
	kas shell --skip setup_home kas/common.yml:kas/machine-{{machine}}.yml:kas/proj-{{project}}.yml


# =========================================================


# Print the path to userland packages
print-userland-path machine="qemu":
	#!/bin/sh
	if [ "{{machine}}" = "qemu"  ]; then echo "build/tmp/work/core2-64-poky-linux"; fi
	if [ "{{machine}}" = "raspi"  ]; then echo "build/tmp/work/aarch64-poky-linux"; fi
	if [ "{{machine}}" = "jetson" ]; then echo "build/tmp/work/aarch64-poky-linux"; fi


# Print the path to kernel packages
print-kernel-path machine="qemu":
	#!/bin/sh
	if [ "{{machine}}" = "qemu"  ]; then echo "build/tmp/work/qemux86_64-poky-linux"; fi
	if [ "{{machine}}" = "raspi"  ]; then echo "build/tmp/work/raspberrypi4_64-poky-linux"; fi
	if [ "{{machine}}" = "jetson" ]; then echo "build/tmp/work/jetson_tx2_devkit-poky-linux"; fi


# Echo the path of selected image which can be of [ qemux | raspi | jetson ]
print-image-path machine="qemu":
	#!/bin/sh
	if [ "{{machine}}" = "qemu"  ]; then echo "build/tmp/deploy/images/qemux86-64"; fi
	if [ "{{machine}}" = "raspi"  ]; then echo "build/tmp/deploy/images/raspberrypi4-64"; fi
	if [ "{{machine}}" = "jetson" ]; then echo "build/tmp/deploy/images/jetson-tx2-devkit"; fi


# =========================================================


# Start Raspi netboot
start-netboot:
	#!/bin/sh
	image_dir="{{invocation_directory()}}/{{`just print-image-path raspi`}}"
	scripts/start-tftp.sh ${image_dir}
	scripts/start-nfs.sh ${image_dir}

# Stop Raspi netboot
stop-netboot:
	@scripts/stop-tftp.sh
	@scripts/stop-nfs.sh
